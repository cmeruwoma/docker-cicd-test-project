FROM ubuntu:18.04
RUN apt-get update -y \
    && apt-get install vim=2:* -y --no-install-recommends \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*